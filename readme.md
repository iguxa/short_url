##Based on AsgardCMS Platform

View the documentation at [AsgardCMS.com/docs](http://asgardcms.com/docs/).

## License

The AsgardCMS is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

## install:
##### git clone git@github.com:iguxa/short_url.git

##### <b>docker</b>
##### docker-compose build
##### docker-compose run node bash //(todo:дать в ручную в баше npm install \ npm run dev)
##### docker-compose run app bash `composer install`
##### docker-compose run app bash  //(todo:дать в ручную в баше php artisan asgard:install \ php artisan  module:migrate \chmod -R 777 storage/)


## optional:
##### npm run watch  
##### php artisan asgard:module:scaffold //create module
##### https://nwidart.com/laravel-modules/v6/advanced-tools/artisan-commands //command for artisan (do not create module on command in this list)
ps/ do not create .env


